# -*- coding: utf-8 -*-
"""
Application de clustering (classifications non supervisées)
Created on Mon Mar 24 10:31:58 2014

@author: thomas_a
"""
from __future__ import print_function
import logging

import numpy as np
import pandas as pd

from sklearn import cluster
from sklearn import metrics
from sklearn.preprocessing import StandardScaler

from maplearn.ml.machine import Machine

logger = logging.getLogger('maplearn.' + __name__)


class Clustering(Machine):
    '''
    Applique des classifications non supervisées à un jeu de données
    - avec des échantillons associées à des données
    - d'autres données
    '''

    def __init__(self, data, algorithms=None, **kwargs):
        if 'metric' in kwargs:
            Machine._params['metric'] = kwargs['metric']
        else:
            Machine._params['metric'] = 'euclidean'
        # n_clusters = nombre de clusters attendus
        if 'n_clusters' in kwargs:
            self.n_cluster = {'f': kwargs['n_clusters']}
            Machine._params['n_clusters'] = kwargs['n_clusters']
        else:
            self.n_cluster = {'f': 15}
        # LISTE DES ALGOS EN STOCK
        Machine.ALL_ALGOS = dict(
            mkmeans=cluster.MiniBatchKMeans(),
            kmeans=cluster.KMeans(n_jobs=-1),
            ms=cluster.MeanShift(bin_seeding=True),
            ward=cluster.AgglomerativeClustering(linkage='ward'),
            spectral=cluster.SpectralClustering(affinity='rbf', eigen_tol=0.25,
                                                eigen_solver='arpack',
                                                assign_labels="kmeans"),
            dbscan=cluster.DBSCAN(),
            propa=cluster.AffinityPropagation(damping=0.8, preference=None), )
        Machine.__init__(self, data, algorithms=algorithms, **kwargs)
        self.algorithms = algorithms
        self.__x = None

    def load(self, data):
        """ Charge les données nécessaires au clustering
        """
        Machine.load(self, data)
        if self._data.data is None:
            self._data.data = self._data.X
        # matrice de distances
        self._data.data = StandardScaler().fit_transform(self._data.data)
        return 0

    def fit_1(self, algo):
        """
        Mesure la qualité d'1 clustering (mais comment ?)
        """
        Machine.fit_1(self, algo)

        self.__x = self._data.data
        try:
            self.clf.fit(self.__x)
        except Exception as ex:
            logger.error(ex)
        else:
            self._fitted = True

    def predict_1(self, algo, export=False):
        """
        clustering
        """
        Machine.predict_1(self, algo)

        if hasattr(self.clf, 'labels_'):
            result = self.clf.labels_.astype(np.int)
        elif hasattr(self.clf, 'predict'):
            result = self.clf.predict(self.__x)
        else:
            result = self.clf.fit_predict(self.__x)

        if hasattr(self.clf, 'score'):
            print('Score : %.2f' % self.clf.score(self.__x))
        n_clusters = np.unique(result).size
        print('%i Clusters expected - Got %i clusters'
            % (self.n_cluster['f'], n_clusters))
        try:
            score = metrics.silhouette_score(self.__x, result,
                                             metric=Machine._params['metric'])
        except MemoryError:
            logger.error('Not enough memory to compute score for %s', algo)
        else:
            print('Silhouette Score : %.2f' % score)

        # 1er resultat ou ajout d'une classification a d'autres resultats
        # existants
        if self.result is None:
            self.result = pd.DataFrame(data=result, columns=[algo, ],
                                       dtype=np.float16)
        else:
            self.result[algo] = result
