# -*- coding: utf-8 -*-
"""
Created on Sun May 27 17:07:11 2012
Test des différentes méthodes de classification supervisées

@author: alban Thomas
"""
from __future__ import print_function
import logging

import numpy as np
import pandas as pd
import pylab as plt
from sklearn.cross_validation import StratifiedKFold, cross_val_score
from sklearn import linear_model, neural_network

from maplearn.ml.machine import Machine

logger = logging.getLogger(__name__)


class Regression(Machine):
    """
    Applique des régressions à un jeu de données
    """

    def __init__(self, data, algorithms=None, **kwargs):
        """
        Initialisation
        """
        Machine.ALL_ALGOS = dict(
            lm=linear_model.LinearRegression(),
            mlp=neural_network.MLPRegressor)
        self.kfold = 3 if 'kfold' not in kwargs else kwargs['kfold']
        Machine.__init__(self, algorithms=algorithms, data=data, **kwargs)

        self.algorithms = algorithms

    def load(self, data):
        """ Charge les données nécessaires à la classification d'un site et
        d'une année
        """
        Machine.load(self, data)
        if self._data.Y is None:
            raise AttributeError('Aucun échantillon spécifié')
        if self._data.data is None:
            self._data.data = self._data.X

    def predict_1(self, algo, proba=False):
        """
        Applique la classification à toute la matrice de données
        """
        Machine.predict_1(self, algo)

        # regression sur toutes les donnees
        self.clf.fit(self._data.X, self._data.Y)
        logger.info('Algorithm %s trained on whole dataset', algo)
        result = self.clf.predict(self._data.data).astype(np.int8)
        logger.info('Prediction on whole dataset with %s', algo)
        # 1er resultat ou ajout d'une classification a d'autres resultats
        # existants
        if self.result is None:
            self.result = pd.DataFrame(data=result, columns=[algo, ],
                                       dtype=np.float16)
        else:
            self.result[algo] = result

        # Rendu Graphique
        plt.plot(self._data.Y, result, '.g', label='samples')
        # plt.show()

    def fit_1(self, algo):
        """
        Mesure la qualité d'1 Régression
        """
        Machine.fit_1(self, algo)
        # Train the model using the training sets
        print('\n####%s - Entrainement Regression####' % algo)
        skf = StratifiedKFold(y=self._data.Y, n_folds=self.kfold, shuffle=True)
        i = 0
        #k = np.zeros(shape=self.kfold)
        for t, v in skf:
            print('\n#####%s (%i/%i)#####' % (algo, i + 1, self.kfold))
            # entrainement sur un k-fold
            try:
                self.clf.fit(self._data.X[t, :], self._data.Y[t])
            except ValueError as e:
                logger.error('%s ne peut être appliqué', algo)
                logger.error(e.message)
                return None
            except (AttributeError, TypeError):
                logger.error('ERREUR dans Regression %s => Exclusion',
                             algo)
                # self._algorithms.pop(name) # exclusion de la classification
                return None

            # estimation
            # The mean square error
            y_pred = self.clf.predict(self._data.X[v, :])
            print("Residual sum of squares: %.2f"
                  % np.mean((y_pred - self._data.Y[v]) ** 2))
            # Explained variance score: 1 is perfect prediction
            print('Variance score: %.2f' % self.clf.score(self._data.X[v, :], self._data.Y[v]))

            i += 1

        scores = cross_val_score(self.clf, self._data.X, self._data.Y)
        mse = cross_val_score(self.clf, self._data.X, self._data.Y,
                              scoring='mean_squared_error')
        rtwo = cross_val_score(self.clf, self._data.X, self._data.Y,
                              scoring='r2')

        print(scores, np.mean(scores), np.std(scores))
        print("mse", mse)
        print("rtwo", rtwo)

        self._fitted = True

    def run(self, predict=False):
        """
        Application de la classificaion suivant toutes les les méthodes
        demandées
        """
        Machine.run(self, predict)

    def optimize(self, algo):
        """
        Optimisation des paramètres d'une régression
        """
        raise NotImplementedError('No optimization available for regression')
