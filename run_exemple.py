# -*- coding: utf-8 -*-
"""
Exemples d'utilisation de la librairie
Created on Thu Nov 14 18:01:28 2013

@author: thomas_a
"""
from __future__ import print_function
import os
import sys
import inspect

# To remove is usefullness confirmed
#if __name__ == '__main__' and __package__ is None:
#    os.sys.path.append(
#        os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

# application path
dir_app = os.path.dirname(os.path.abspath(
    inspect.getfile(inspect.currentframe())))
if __name__ == '__main__' and __package__ is None:
    os.sys.path.append(dir_app)

#from maplearn.app.config import Config

EX = ("""
      Classification supervisée à partir d'un shapefile

      * Les échantillons sont chargés à partir de la table attributaire du
      fichier 'echantillon.shp', avec la colonne 'ECH' qui contient les
      libellés des classes.
      * les données à classer sont dans un second fichier ('data.shp')

      3 algorithmes de classification sont comparés :
          - un arbre de décision simple (tree)
          - k nearest neighbour (knn)
          - Linear Discriminant analysis (lda)

      Le résultat est écrit dans un nouveau shapefile 'sortie.shp'
      """,
      """
      Lecture d'un csv avec un dictionnaire de codes

      Les échantillons sont lus à partir d'un fichier csv, avec la colonne
      'Id_classes' qui indique les numéros de classes. Ces identifiants sont
      associés à des libellés grâce à un dictionnaire écrit dans le code

      Lors du pretraitement, les meilleures features sont sélections

      La classification se fait par SVM, avec une recherche des paramètres
      optimaux
      """,
      """
      Jeu de données test

      Cet exemple utilise un des deux jeux de données test reconnus en
      traitement du signal : iris et digits

      La separabilité des échantillons est analysée en prétraitement

      Les algorithmes de classification lda, knn et svm sont comparés, après
      avoir recherchés les meilleurs paramètres possibles
      """,
      """
      Test des nouveaux algorithmes de classification
      """,
      """
      Classification non supervisée à partir d'un shapefile
      """,
      """
      Classification non supervisée d'une image satellite
      """,
      """
      Classification supervisée d'une image satellite
      """)


def run_example(number, path):
    """
    Execute un exemple donné
    """
    str_msg = 'python %s -c %s' % \
              (os.path.join(path, "run.py"),
               os.path.join(path, "examples", "example%s.cfg" % number))
    os.system(str_msg)

if __name__ == '__main__':
    if __package__ is None:
        os.sys.path.append(dir_app)
    #cfg = Config('config.cfg')
    idx_ex = [i for i in range(len(EX))]
    for i in idx_ex:
        print('*' * 80)
        print("EXEMPLE %i : %s" % (i + 1, EX[i]))

    idx_ex = [str(i + 1) for i in idx_ex]
    MSG = 'Choisissez un exemple (%s-%s) :' % (idx_ex[0], idx_ex[-1])
    #PYTHON2/3 comptability
    try:
        reponse = raw_input(MSG).strip()
    except NameError:
        reponse = input(MSG).strip()
    again = True
    while again:
        again = False
        if reponse == 'all':
            for i in idx_ex:
                run_example(i, dir_app)
        if reponse == 'q':
            sys.exit(0)
        elif reponse in idx_ex:
            run_example(reponse, dir_app)
        else:
            again = True
            #PYTHON2/3 comptability
            try:
                reponse = raw_input(MSG).strip()
            except NameError:
                reponse = input(MSG).strip()

